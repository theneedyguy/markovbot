package main

import (
	"fmt"
	"log"
	"math/rand"
	"os"
	"strconv"
	"strings"
	"time"

	"github.com/MemeLabs/dggchat"
	gptclient "github.com/go-skynet/llama-cli/client"
)

func gptMsg(m dggchat.Message, s *dggchat.Session) {
	cmd := strings.Split(m.Message, " ")
	cli := gptclient.NewClient(gptcontainer)
	log.Printf("Generating GPT Message. Waiting...")
	out, err := cli.Predict(strings.Join(cmd[1:], " "))
	if err != nil {
		log.Printf("An error occurred: %v\n", err)
	}
	log.Printf("GPT message: '%s'\n", out)
	if isGamerWord(out) {
		_ = s.SendMessage("Unfortunately the message the model generated contained some gamer words. I can not post this shit here.")
	} else {
		_ = s.SendMessage(out)
	}
}

// This function expects a dggchat Message and a pointer to a dggchat Session
// It can read the message object and do something with it.
// Here it is used to do some command checks
func handleCommand(m dggchat.Message, s *dggchat.Session) {
	// This handles the sleep command
	// If an admin triggers the command it will send a message to chat and toggle the sleep variable
	if strings.HasPrefix(m.Message, "!sleep") {
		if isAdmin(m.Sender.Nick) {
			isSleeping := toggleSleep()
			if isSleeping {
				_ = s.SendMessage("Sleeping.")
			} else {
				_ = s.SendMessage("Ready to meme!")
			}

		}
	}
	if strings.HasPrefix(m.Message, "!gpt") {
		if isAdmin(m.Sender.Nick) {
			go gptMsg(m, s)
		}
	}
	// This handles the debug command
	// It just writes variables related to the bots speed to the console
	if strings.HasPrefix(m.Message, "!debug") {
		if isAdmin(m.Sender.Nick) {
			u := s.GetUsers()
			log.Printf("Debug output: Interval (%v), SpeedTime (%v), Threshold (%v), Users (%v)", messageInterval, speedTime, msgThreshold, u)
		}
	}
	if strings.HasPrefix(m.Message, "!contextmode") {
		if isAdmin(m.Sender.Nick) {
			//FAST
			chainSpeed.Set(3)
			speedTime = time.Duration(30)
			messageInterval = time.Second * speedTime
			msgThreshold = 15
			log.Println("Context mode triggered")
			toggleContext()
		}
	}
	//
	if strings.HasPrefix(m.Message, "!reset") {
		if isAdmin(m.Sender.Nick) {
			// Kill the program with code 1 to make Docker handle restarts
			os.Exit(1)
		}
	}
	// !chain <username> generates a random markov chain of the usernames recent logs
	if strings.HasPrefix(m.Message, "!chain") {
		if !allowChainCmd {
			_ = s.SendPrivateMessage(m.Sender.Nick, "The container that processes logs is not configured. Please contact the admin of this bot and ask him to configure it properly.")
		} else {
			if isAdmin(m.Sender.Nick) {
				cmd := strings.Split(m.Message, " ")
				nick := cmd[1]
				result := doLogRequest(nick)
				_ = s.SendMessage(result)
			}
		}
	}
	if strings.HasPrefix(m.Message, "!replytime") {
		if isAdmin(m.Sender.Nick) {
			cmd := strings.Split(m.Message, " ")
			interval := cmd[1]
			i, err := strconv.Atoi(interval)
			if err != nil {
				_ = s.SendMessage("Expected integer value for seconds.")
				log.Printf("Invalid replytime second type\n")
			} else {
				trueInterval = time.Second * time.Duration(i)
				log.Printf("Set replytime to: (%v)\n", trueInterval)
			}
		}
	}
	// This handles the speed modifier
	// It splits the message that begins with !speed to an array
	// Then it converts the seconds member of the array to a number
	// If it fails then nothing will happen
	// If it succeeds to convert it to a number then this number sets the speed of the bot
	if strings.HasPrefix(m.Message, "!speed") {
		if isAdmin(m.Sender.Nick) {
			cmd := strings.Split(m.Message, " ")
			speed := cmd[1]
			i, err := strconv.Atoi(speed)
			if err == nil {
				switch i {
				case 0:
					//SLOW
					chainSpeed.Set(0)
					speedTime = time.Duration(300)
					messageInterval = time.Second * speedTime
					msgThreshold = 100
					log.Println("Speed setting: slow")
				case 1:
					//MEDIUM
					chainSpeed.Set(1)
					speedTime = time.Duration(150)
					messageInterval = time.Second * speedTime
					msgThreshold = 50
					log.Println("Speed setting: medium")
				case 2:
					//FAST
					chainSpeed.Set(2)
					speedTime = time.Duration(60)
					messageInterval = time.Second * speedTime
					msgThreshold = 20
					log.Println("Speed setting: fast")
				case 3:
					//FAST
					chainSpeed.Set(3)
					speedTime = time.Duration(30)
					messageInterval = time.Second * speedTime
					msgThreshold = 10
					log.Println("Speed setting: ultrafast")
				}
			} else {
				log.Printf("An error occurred when setting the speed: %v", err)
			}
		}
	}
}

// Toggles the botSleep boolean to change values
// Returns the new state as boolean
func toggleSleep() bool {
	if !botSleep {
		botSleep = true
		return botSleep
	}
	botSleep = false
	return botSleep
}

func answerTrue(nick string) string {
	answers := []string{
		"-1000",
		"-100",
		"+100",
		"+69",
		"-10000000",
		"+1000",
		"TRUE! LULW",
		"NOT FALSE LULW",
		"yeah, pretty true...",
		"OMG! TRUE!",
		"FACTUALLY ACCURATE",
		"Not incorrect!",
		"Shut up, dumbass!",
	}
	rand.Seed(time.Now().Unix())
	return fmt.Sprintf("%s %s", nick, answers[rand.Intn(len(answers))])
}

// Toggles the contextMode boolean to change values
// Returns the new state as boolean
func toggleContext() bool {
	if !contextMode {
		contextState.Set(1)
		contextMode = true
		return contextMode
	}
	contextState.Set(0)
	contextMode = false
	return contextMode
}

// Expects a string as input and runs is through the isInList function
// Returns true if the poster is an admin and false if not
func isAdmin(s string) bool {
	return isInList(s, admins)
}

// Expects a string as input and runs is through the isInList function
// Returns true if the poster is an ignored user and false if not
func isIgnored(s string) bool {
	return isInList(strings.ToLower(s), ignores)
}

// Expects a string as input
// Splits the string into words and loops through the array of words
// It then runs each word through the isInList function
// Returns true if the word is a gamer word and false if it is not
func isGamerWord(s string) bool {
	words := strings.Split(s, " ")
	for _, word := range words {
		if isInList(word, gamerwords) {
			log.Printf("A filtered word was detected: '%s'", word)
			return true
		}
	}
	return false
}

// This function expects a string and a string array
// It then loops the strings through the array an compares the string with each member of the array
// Returns true if word is found in array and false if not
func isInList(s string, list []string) bool {
	for _, v := range list {
		if strings.EqualFold(strings.ToLower(s), strings.ToLower(v)) {
			return true
		}
	}
	return false
}
