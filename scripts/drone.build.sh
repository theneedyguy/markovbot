#!/bin/sh

REPO="gitlab.com/theneedyguy/markovbot"

go get ${REPO}
go build -a -v -installsuffix cgo -o release/${GOOS}/${GOARCH}/markovbot ${REPO}
