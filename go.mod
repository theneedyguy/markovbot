module gitlab.com/theneedyguy/markovbot

go 1.20

require (
	github.com/MemeLabs/dggchat v0.0.0-20201117114323-43344edb4906
	github.com/go-skynet/llama-cli v0.0.0-20230321231848-0785cb6b0be0
	github.com/mb-14/gomarkov v0.0.0-20210216094942-a5b484cc0243
	github.com/prometheus/client_golang v1.6.0
)

require (
	github.com/beorn7/perks v1.0.1 // indirect
	github.com/cespare/xxhash/v2 v2.1.1 // indirect
	github.com/golang/protobuf v1.4.0 // indirect
	github.com/gorilla/websocket v1.4.2 // indirect
	github.com/matttproud/golang_protobuf_extensions v1.0.1 // indirect
	github.com/prometheus/client_model v0.2.0 // indirect
	github.com/prometheus/common v0.9.1 // indirect
	github.com/prometheus/procfs v0.0.11 // indirect
	golang.org/x/sys v0.6.0 // indirect
	google.golang.org/protobuf v1.21.0 // indirect
)
