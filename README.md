# Markov Bot (tng69)

[![Build Status](https://drone.0x01.host/api/badges/root/theneedyguy-markovbot/status.svg?ref=master)](https://drone.0x01.host/root/theneedyguy-markovbot)

This is a bot that generates Markov Chains based on what it reads in [destiny.gg](https://destiny.gg) and posts them in chat on random intervals (but not too often to make it the center of attention)


## Admins:
- theneedyguy
- RightToBearArmsLOL
- Destiny
- Cake

## Commands (Admins only):

- !sleep > Stops all chat output
- !speed [0-3]
- !reset > Exits with exit code 1 to restart container
- !debug > Writes some data to stdout. Only useful to quickly check status.
- !contextmode
- !replytime [Seconds] > Sets TRUE reply to the desired amount of seconds

## Context Mode

Resets the chain every 150 chat lines. Keeps the current convo context current. Use only when you want to meme out.

## Speeds:

**0 = Slow, 1 = Medium, 2 = Fast, 3 = Ultrafast**

- 0 = 5:00 Minutes AND 100 Messages before post
- 1 = 2:30 Minutes AND 50 Messages before post
- 2 = 1:00 Minute AND 20 Messages before post
- 3 = 30 Seconds AND 10 Messages before post
