# Changelog

## 1.4.1- 2019-12-03

### Added

- Changed output of chain to include a random date to make it look like it was a log.

## 1.4.0 - 2019-11-27

### Added

- Added !chain command. [ADMIN ONLY]

## 1.3.1 - 2019-11-26

### Changed

- Cleaned up logging

## 1.3.0 - 2019-11-26

### Added

- Added drone-ci pipeline for automatic builds
- Changelog of older releases is not tracked so I now added this
