package main


import (
	"net/http"
	"encoding/json"
	"log"
	"errors"
	"io"
	"regexp"
	"github.com/mb-14/gomarkov"
	"strings"
)

// The response struct
type response struct {
	Msg string `json:"msg"`
	ECode int `json:"ecode"`
        Nick string `json:"nick"`
	Date string `json:"date"`
}


func getChatLines(lines []string) ([]string, error) {
	reg := regexp.MustCompile(`\[.*\]\s{1}.*:\s{1}`)
	chatLogs := []string{}
	for i := range lines {
		val := reg.Split(lines[i], -1)
		//fmt.Printf("%v\n", val[len(val)-1])
		chatLogs = append(chatLogs, val[len(val)-1])
	}
	if len(chatLogs) < 20 {
		notEnoughLinesErr := errors.New("Not enough lines to work with for this month.")
		return chatLogs, notEnoughLinesErr
	}
	return chatLogs, nil

}

func genMsg(lines []string) string {
	chain := gomarkov.NewChain(1)
	for i := range lines {
		// Split up chatline into words by splitting on each space
		chain.Add(strings.Split(lines[i], " "))
	}

	tokens := []string{gomarkov.StartToken}
	for tokens[len(tokens)-1] != gomarkov.EndToken {
		next, _ := chain.Generate(tokens[(len(tokens) - 1):])
		tokens = append(tokens, next)
	}
	result := strings.Join(tokens[1:len(tokens)-1], " ")
	return string(result)

}

func logHandler(res http.ResponseWriter, req *http.Request) {
	if req.Method == "POST" {
		nick := req.FormValue("nick")
		var months []interface{} = getMonths()
		// We only want the most recent month to keep it kinda in the context of time
		for i := range months[len(months)-1: len(months)] {
			var lines, err = getChatLines(getMonthLogs(months[i], nick))
			if err == nil {
				result := genMsg(lines)
				date := randDate()
				a := &response{Msg: result, Nick: nick, ECode: 0, Date: date.Format("2006-01-02 15:04:05 UTC")}
				out, err := json.Marshal(a)
				if err != nil {
					log.Fatalln(err)
				}
				io.WriteString(res, string(out))
			} else {
				a := &response{Msg: "Error", Nick: nick, ECode: 1}
				out, err := json.Marshal(a)
				if err != nil {
					log.Fatalln(err)
				}
				io.WriteString(res, string(out))
			}
		}
	} else {
		res.WriteHeader(http.StatusForbidden)
	}
}

