#!/bin/sh

REPO="gitlab.com/theneedyguy/markovbot/cmd/logmarkov"

go get ${REPO}
go build -a -v -installsuffix cgo -o release/${GOOS}/${GOARCH}/logmarkov ${REPO}
