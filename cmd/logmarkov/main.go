package main


import (
	"net/http"
	"encoding/json"
	"log"
	"fmt"
	"io/ioutil"
	"strings"
	"time"
	"math/rand"
)


// Entrypoint to handle
func main() {
	http.HandleFunc("/logs", func(res http.ResponseWriter, req *http.Request) {
		res.Header().Set("Cache-Control", "no-store, no-cache, must-revalidate, post-check=0, pre-check=0")
		res.Header().Set("Content-Type", "application/json")
		logHandler(res, req)
	})
	log.Fatal(http.ListenAndServe(":1111", nil))
}

// Query the overrustle api for the months api
// Returns an interface slice
func getMonths() []interface {} {
	resp, err := http.Get("https://overrustlelogs.net/api/v1/Destinygg/months.json")
	if err != nil {
		log.Fatalln(err)
	}
	var result []interface{}
	json.NewDecoder(resp.Body).Decode(&result)
	return result
}

// Query a user's logs for a certain month
// Requires an interface and a string
// Returns a string slice
func getMonthLogs(month interface{}, user string) []string {
	resp, err := http.Get(fmt.Sprintf("https://overrustlelogs.net/Destinygg chatlog/%v/userlogs/%s.txt", month, user))
	if err != nil {
		log.Fatalln(err)
	}
	bodyBytes, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		log.Fatal(err)
	    }
	bodyString := string(bodyBytes)
	if strings.HasPrefix(bodyString, "didn't find any logs for this user") {
		return []string{}
	} else {
		lines := strings.Split(bodyString, "\n")
		return lines
	}
}

// Generates a random time to be included in the api response
func randDate() time.Time {
	min := time.Date(2014, 1, 0, 0, 0, 0 ,0, time.UTC).Unix()
	max := time.Now().Unix()
	delta := max - min
	sec := rand.Int63n(delta) + min
	return time.Unix(sec,0)
}
