# DGG Nword Counter

## What is this?

This a small web API that you can run locally generate a random markov chain from a chatters recent logs.

## Usage

To run it you have to have Go (Programming language) installed and type the following into your command line:

```sh
go run main.go
```

### Unix

Once it is up it is listening on port 1111.
To query a user (in this example Destiny) enter the following command (Linux / MacOS):

```sh
curl -x POST http://localhost:1111/logs?nick=Destiny
```

### Windows

In Windows run the following in PowerShell:

```powershell
Invoke-Webrequest -Method Post -Uri "http://localhost:1111/logs?nick=Destiny"
```


