package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"time"
)

const (
	url      = "https://www.destiny.gg/api/info/stream"
	interval = 120
)

var (
	apiKey = os.Getenv("TWITCH_KEY")
)

type response struct {
	Msg   string `json:"msg"`
	ECode int    `json:"ecode"`
	Nick  string `json:"nick"`
	Date  string `json:"date"`
}

type DGGStream struct {
	Data struct {
		Streams struct {
			Twitch struct {
				Live bool `json:"live"`
			} `json:"twitch"`
			Youtube struct {
				Live bool `json:"live"`
			} `json:"youtube"`
		} `json:"streams"`
	} `json:"data"`
}

func (dggStream DGGStream) isLive() bool {
	if dggStream.Data.Streams.Twitch.Live || dggStream.Data.Streams.Youtube.Live {
		return true
	} else {
		return false
	}
}

// This function will be run as a goroutine indefinitely
// It periodically connects to twitch via a library to check whether destiny is online
// if the returned array is longer than 0 then the stream is on and the streaming variable gets changed
func doRequest() {
	for {
		client := http.Client{
			Timeout: time.Second * 60, // Maximum of 60 secs
		}
		req, err := http.NewRequest(http.MethodGet, url, nil)
		if err != nil {
			log.Fatalln(err)
		}
		req.Header.Set("User-Agent", "tng69-BOT")

		res, err := client.Do(req)
		if err != nil {
			log.Fatalln(err)
		}
		defer res.Body.Close()

		body, err := ioutil.ReadAll(res.Body)
		if err != nil {
			log.Fatalln(err)
		}
		responseBody := DGGStream{}
		jsonErr := json.Unmarshal(body, &responseBody)
		if jsonErr != nil {
			log.Fatalln(jsonErr)
		}
		streaming = responseBody.isLive()
		if streaming {
			streamLive.Set(1)
		} else {
			streamLive.Set(0)
		}
		log.Printf("Stream live? %t\n", streaming)
		time.Sleep(interval * time.Second)
	}
}

func doLogRequest(nick string) string {
	client := http.Client{
		Timeout: time.Second * 60, // Maximum of 60 secs
	}
	req, err := http.NewRequest(http.MethodPost, fmt.Sprintf("http://%s:1111/logs?nick=%s", logContainerName, nick), nil)
	if err != nil {
		log.Fatalln(err)
	}
	req.Header.Set("User-Agent", "tng69-BOT")

	res, err := client.Do(req)
	if err != nil {
		log.Fatalln(err)
	}
	defer res.Body.Close()

	body, err := ioutil.ReadAll(res.Body)
	if err != nil {
		log.Fatalln(err)
	}

	responseBody := response{}
	jsonErr := json.Unmarshal(body, &responseBody)
	if jsonErr != nil {
		log.Fatalln(jsonErr)
	}
	if responseBody.ECode > 0 {
		log.Println("There was not enough data to create a decent markov chain.")
	}
	return fmt.Sprintf("[%s] %s: %s", responseBody.Date, responseBody.Nick, responseBody.Msg)

}
