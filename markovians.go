package main

import (
	"log"
	"strings"

	"github.com/mb-14/gomarkov"
)

// This function expects a markov chain that it then uses to generate a message
// tng69 is harcoded to be filteres out so the bot does not mention itself
// In the future this might be a config file option.
func genMsg(markov *gomarkov.Chain) string {
	tokens := []string{gomarkov.StartToken}
	for tokens[len(tokens)-1] != gomarkov.EndToken {
		next, _ := markov.Generate(tokens[(len(tokens) - 1):])
		tokens = append(tokens, next)
	}
	result := strings.Join(tokens[1:len(tokens)-1], " ")
	log.Printf("Generated message: '%s'\n", result)
	return strings.Replace(result, "tng69", "", -1)
}
