package main

import (
	"log"
	"net/http"

	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promhttp"
)

// Metrics have to be registered to be exposed:
func init() {
	prometheus.MustRegister(chainMetric)
	prometheus.MustRegister(chainSent)
	prometheus.MustRegister(chainSpeed)
	prometheus.MustRegister(omegalulCounter)
	prometheus.MustRegister(contextState)
	prometheus.MustRegister(streamLive)
}

// The Handler function provides a default handler to expose metrics
// via an HTTP server. "/metrics" is the usual endpoint for that.
func runMetrics() {
	http.Handle("/metrics", promhttp.Handler())
	log.Fatal(http.ListenAndServe(":9999", nil))
}
