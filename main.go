package main

import (
	"encoding/json"
	"io/ioutil"
	"log"
	"os"
	"os/signal"
	"strings"
	"syscall"
	"time"
	"unicode"

	"github.com/MemeLabs/dggchat"
	"github.com/mb-14/gomarkov"
	"github.com/prometheus/client_golang/prometheus"
)

const (
	defaultConfigFile = "/opt/markov/config.json"
	//defaultConfigFile = "./config.json"
	pingInterval = time.Minute
)

var (
	botVersion       = "1.7.3"
	graphMessage     = 0
	botSleep         = false
	contextMode      = false
	lastSent         = time.Now()
	lastTrueSent     = time.Now()
	startTime        = time.Now()
	lastPing         = timeToUnix(time.Now())
	lastPong         = timeToUnix(time.Now())
	speedTime        = time.Duration(300)
	speedTrue        = time.Duration(300)
	streaming        = false
	msgThreshold     = 100
	messageInterval  = time.Second * speedTime
	trueInterval     = time.Second * speedTrue
	configFile       string
	admins           []string
	ignores          []string
	gamerwords       []string
	gptcontainer     string
	graphURL         string
	key              string
	chain            = gomarkov.NewChain(1)
	logContainerName string
	allowChainCmd    = false
	chainSize        = 0
	contextSize      = 0
	contextState     = prometheus.NewGauge(prometheus.GaugeOpts{
		Name: "dgg_context_state",
		Help: "Displays if context mode is enabled (0 = off, 1 = on)",
	})
	chainSpeed = prometheus.NewGauge(prometheus.GaugeOpts{
		Name: "dgg_chain_speed",
		Help: "Current Chain Size",
	})
	chainMetric = prometheus.NewCounter(prometheus.CounterOpts{
		Name: "dgg_chain_size",
		Help: "Current Chain Size",
	})
	omegalulCounter = prometheus.NewCounter(prometheus.CounterOpts{
		Name: "dgg_omegalul_farm_size",
		Help: "Current Chain Size",
	})
	chainSent = prometheus.NewCounter(prometheus.CounterOpts{
		Name: "dgg_chain_sent",
		Help: "Times a chain has been sent",
	})
	streamLive = prometheus.NewGauge(prometheus.GaugeOpts{
		Name: "dgg_stream_live",
		Help: "Shows whether the stream is live",
	})
)

type (
	config struct {
		Key              string   `json:"login_key"`
		Admins           []string `json:"admins"`
		Ignores          []string `json:"ignores"`
		GamerWords       []string `json:"gamerwords"`
		Graph            string   `json:"graph"`
		LogContainerName string   `json:"logcontainername"`
		GPTContainer     string   `json:"gptcontainer"`
	}

	apiResp struct {
		URL string `json:"url"`
	}
)

// Entrypoint of the bot
// Configs get set and routines start here
func main() {
	var file string
	if len(os.Args) < 2 {
		file = defaultConfigFile
	} else {
		file = os.Args[1]
	}

	f, err := ioutil.ReadFile(file)
	if err != nil {
		log.Fatalln(err)
	}

	var c config
	err = json.Unmarshal(f, &c)
	if err != nil {
		log.Fatalf("Error: %v", err)
	}

	configFile = file

	if c.Key == "" {
		log.Fatalln("No d.gg login key provided")
	}

	key = c.Key

	if c.Admins == nil {
		c.Admins = make([]string, 0)
	}

	admins = c.Admins

	if c.Ignores == nil {
		c.Ignores = make([]string, 0)
	}

	ignores = c.Ignores

	if c.GamerWords == nil {
		c.GamerWords = make([]string, 0)
	}

	gamerwords = c.GamerWords

	gptcontainer = c.GPTContainer

	if c.Graph == "" {
		log.Println("No Graph URL provided")
		graphURL = ""
	} else {
		graphURL = c.Graph
	}
	if c.LogContainerName == "" || len(c.LogContainerName) == 0 {
		log.Println("'logcontainername' not set in config. !chain command will not be available.")
	} else {
		logContainerName = c.LogContainerName
		allowChainCmd = true
	}
	go startBot(c.Key)
	go runMetrics()
	go doRequest()

	sc := make(chan os.Signal, 1)
	signal.Notify(sc, syscall.SIGINT)
	<-sc
}

// This is the heart of the "bot"
// Here messages, pings and errors are handled.
func startBot(key string) {
	dgg, err := dggchat.New(key)
	log.Println("Set auth key")
	if err != nil {
		log.Println("Shit")
		log.Fatalln(err)
	}

	err = dgg.Open()
	if err != nil {
		log.Fatalf("Error in bot: %v", err)
	}

	defer dgg.Close()

	messages := make(chan dggchat.Message)
	errors := make(chan string)
	pings := make(chan dggchat.Ping)

	dgg.AddMessageHandler(func(m dggchat.Message, s *dggchat.Session) {
		messages <- m
	})

	dgg.AddErrorHandler(func(e string, s *dggchat.Session) {
		errors <- e
	})

	dgg.AddPingHandler(func(p dggchat.Ping, s *dggchat.Session) {
		pings <- p
	})
	go checkConnection(dgg)

	// This is really ugly and not optimized but it works for the bot
	for {
		select {
		case m := <-messages:
			// First we check if the message contains ASCII chars. If yes we don't add it to the chain.
			// If it is okay we move on to other checks
			if !isASCII(m.Message) {
				log.Printf("Non-ascii characters in message detected and ignored: %s\n", m.Message)
			} else {
				// If the chain size not reached and not enough time has elapsed we keep adding to the chain if the messages are valid
				timeElapsed := time.Since(lastSent)
				if !(IsChainReady() && IsTimeReady(timeElapsed)) {
					// Check if either the message comes from a ignores nick or contains gamerwords
					if !(isIgnored(m.Sender.Nick) || isGamerWord(strings.ToLower(m.Message))) {
						chainMetric.Inc()
						chain.Add(strings.Split(m.Message, " "))
						chainSize = chainSize + 1
						contextSize = contextSize + 1
					}
					// If we can post then we post it BOIIII
				} else {
					// Check if bot is sleeping and if stream is on before we post if all is good we post a message
					if !botSleep {
						if !streaming {
							if !contextMode {
								mesg := genMsg(chain)
								_ = dgg.SendMessage(mesg)
								chainSent.Inc()
								chainSize = 0
								lastSent = time.Now()
							} else {
								if contextSize >= 150 {
									chainSize = 0
									contextSize = 0
									chain = gomarkov.NewChain(1)
								} else {
									mesg := genMsg(chain)
									_ = dgg.SendMessage(mesg)
									chainSent.Inc()
									chainSize = 0
									lastSent = time.Now()
								}
							}
						}
					}
				}
			}
			// Handle commands with a function that expects a message and the dgg session
			if strings.HasPrefix(m.Message, "!") {
				handleCommand(m, dgg)
			}
			if strings.Contains(m.Message, "tng69") {
				if strings.Contains(m.Message, "OMEGALUL") {
					omegalulCounter.Inc()
				}
				timeElapsed := time.Since(lastTrueSent)
				if IsTrueReady(timeElapsed) && !botSleep {
					log.Println(answerTrue(m.Sender.Nick))
					_ = dgg.SendMessage(answerTrue(m.Sender.Nick))
					lastTrueSent = time.Now()
				}
			}
		case e := <-errors:
			log.Printf("Error %s\n", e)
		case p := <-pings:
			lastPong = p.Timestamp
		}
	}

}

// This is a helper function to check if the chain is long enough to fullfill the threshold requirements
// Returns true if the chain is sufficiently long enough
func IsChainReady() bool {
	if chainSize < msgThreshold {
		return false
	} else {
		return true
	}
}

// This helper function helps check if enough time has passed for a message to be posted
// It expects a duration and returns true if enough time has passed.
func IsTimeReady(timeElapsed time.Duration) bool {
	if timeElapsed < messageInterval {
		return false
	} else {
		return true
	}
}
func IsTrueReady(timeElapsed time.Duration) bool {
	if timeElapsed < trueInterval {
		return false
	} else {
		return true
	}
}

// This function expects a string and loops through it
// Each character gets checked if it has a value higher than the maximum of ascii ('\u007F')
// If it is an ascii char then it returns true otherwise it returns false
func isASCII(s string) bool {
	for i := 0; i < len(s); i++ {
		if s[i] > unicode.MaxASCII {
			return false
		}
	}
	return true
}

// This function regularly sends pings to the chat backend to keep the connection alive
func checkConnection(s *dggchat.Session) {
	ticker := time.NewTicker(pingInterval)
	for {
		<-ticker.C
		if lastPing != lastPong {
			log.Println("Ping mismatch, attempting to reconnect")
			err := s.Close()
			if err != nil {
				log.Fatalln(err)
			}

			err = s.Open()
			if err != nil {
				log.Fatalln(err)
			}

			continue
		}
		s.SendPing()
		lastPing = timeToUnix(time.Now())
	}
}

// This function converts a time object to unixtime
func timeToUnix(t time.Time) int64 {
	return t.Unix() * 1000
}
